# Arch Package Builds

Use standard tools to build Arch Linux packages.

## Repository

This repository has a `common` branch, used to include all the common packages
between several architectures (plus helper scripts), and a branch per
architecture.

This is done to only check out architecture-specific packages on the
corresponding machines. Packages that support `any` architecture can be
committed to the `common` branch.

## Usage

The `devtools` package is a hard dependency.

### Setup the chroot

See:
- https://wiki.archlinux.org/index.php/DeveloperWiki:Building_in_a_clean_chroot#Setting_up_a_chroot

Put it in `$CHROOT`:

```sh
$ export CHROOT="$HOME/.cache/archroots"
$ mkdir -p "$CHROOT"
$ mkarchroot "$CHROOT/root" base-devel
```

That `$CHROOT` should go into the default shell profile.

## Configure the packager information and GPG key

See:
- https://wiki.archlinux.org/index.php/Makepkg#Configuration
- https://wiki.archlinux.org/index.php/DeveloperWiki:Signing_Packages
- https://wiki.archlinux.org/index.php/GnuPG#Exporting_subkey

Make sure to setup GPG and create a key, and ensure the email for the key is
the same email configured as the `PACKAGER`.

Either:
- Generate a new key (can be sign-only), and publish it
- From your existing key, create a sign-only subkey and import it into the local keyring

Configure makepkg to use it:

```sh
$ mkdir -p ~/.config/pacman
$ gpg --list-secret-keys  # Use a "ssb" with "E" as KEYID
$ cat >~/.config/pacman/makepkg.conf <<EOF
PACKAGER="NAME <EMAIL>"
GPGKEY=KEYID
EOF
```

### Get the packages

Clone this repository recursively somewhere and use it.

#### Publish the public key among the package files

```sh
$ gpg --armor --export KEYID >"repo/$REPOSITORY.pub"
```

#### Configure the machine to trust packages signed with this key

Note this needs to run as root, since it implies trusting packages signed with this key.

```
# pacman-key --add "repo/$REPOSITORY.pub"
# pacman-key --lsign-key KEYID
```
