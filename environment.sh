#!/bin/bash
# This is to be sourced, not executed
current_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Configuration
configuration="$current_dir/config.sh"
if [ ! -r "$configuration" ]; then
	echo "ERROR: Missing configuration file @ '$configuration'" >&2
else
	export ROOT="$current_dir"  # TODO: Use git toplevel directory?
	echo "=> Current Root Folder: $(realpath --relative-to="$PWD" "$ROOT")"
	echo "=> Sourcing configuration variables"
	# shellcheck disable=SC1090
	source "$current_dir/config.sh"
	if [ -z "$REPOSITORY" ] || [ -z "$CHROOT" ]; then
		echo "ERROR: Missing variables (REPOSITORY CHROOT)" >&2
	else
		# PATH manipulation
		echo "=> Adding binaries to PATH"
		PATH="$current_dir/bin:$PATH"
		# Other variables
		export REPOSITORY CHROOT
		if [ ! -d "$REPO_FOLDER" ]; then
			export REPO_FOLDER="$ROOT/repo"
		fi
		export REMOVED_PACKAGES="$ROOT/removed"
	fi
fi
